//
//  CursosController.swift
//  Cursos
//
//  Created by carlos pumayalla on 10/12/21.
//  Copyright © 2021 empresa. All rights reserved.
//

import UIKit

class CursosController: UIViewController {
    
    var anteriorVC = ViewController()
    @IBOutlet weak var txtNombreCurso: UITextField!
    @IBOutlet weak var txtLabs: UITextField!
    @IBOutlet weak var txtPractica: UITextField!
    @IBOutlet weak var txtExam: UITextField!
    @IBAction func agregar(_ sender: Any) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let curso = Curso(context: context)
        
        curso.nombre = txtNombreCurso.text!
        curso.nota_lab = Double(txtLabs.text!)!
        curso.nota_practica = Double(txtPractica.text!)!
        curso.exam_final = Double(txtExam.text!)!
        curso.promedio = (Double(txtLabs.text!)! * 0.6) + (Double(txtPractica.text!)! * 0.3) + (Double(txtExam.text!)! * 0.1)
        
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        navigationController?.popViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
