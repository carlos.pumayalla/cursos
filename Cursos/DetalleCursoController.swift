//
//  DetalleCursoController.swift
//  Cursos
//
//  Created by carlos pumayalla on 10/12/21.
//  Copyright © 2021 empresa. All rights reserved.
//

import UIKit

class DetalleCursoController: UIViewController {
    
    var curso:Curso? = nil
    
    @IBOutlet weak var lblCurso: UILabel!
    @IBOutlet weak var lblPromedio: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        lblCurso.text = curso!.nombre!
        let promedio = String(curso!.promedio)
        lblPromedio.text = "Promedio Final: \(promedio)"
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
